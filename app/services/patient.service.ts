import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';

import {Patient} from './patient';


@Injectable()
export class PatientService {

    constructor(private http: Http) { }
    //return $http.get(apiEndpoint.baseUrl + 'dischargeservice/api/provider/referralPatient/' + id)

    private _patientUrl = '/dischargeservice/api/provider/referralPatient/1583';
    private _gitUserUrl = 'https://api.github.com/users';

    getPatient() {
        return this.http.get(this._patientUrl)
                        .map(res => <Patient>res.json())
                        .catch(this.handleError);
    }

    getGitUser() {
        return this.http.get(this._gitUserUrl)
                        .map(res => <any[]>res.json())
                        .do(data => console.log(data))
                        .catch(this.handleError);
    }

    getGitRandomUsers() {
        var randomNumber = Math.floor(Math.random()*500);
        var randomUrl = 'https://api.github.com/users?since=' + randomNumber;

        return this.http.get(randomUrl)
                        .map(res => <any[]>res.json())
                        .catch(this.handleError);
    }

    handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}