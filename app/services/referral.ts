import {ReferralStatus} from "./referralStatus";
import * as moment from "moment";

export class Referral {
    public name: string;
    public hospital: string;
    public dischargeDates: string;
    public status: ReferralStatus;
    public createdDate: any;
}