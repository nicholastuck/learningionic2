export class Planner {
    public id: number;
    public firstName: string;
    public lastName: string;
    public username: string;
    public mobilePhone: number;
    public workPhone: number;
    public email: string;
}