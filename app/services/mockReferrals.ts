import {Referral} from './referral'
import {ReferralStatus} from './referralStatus';
import * as moment from "moment";

//noinspection TypeScriptUnresolvedFunction
export var REFERRALS: Referral[] = [
    {
        name: 'Elva Lynch',
        hospital: 'Bergan Mercy Medical Center',
        dischargeDates: '4/8/16 - 4/12/16',
        status: ReferralStatus.Open,
        createdDate: moment()
    },
    {
        name: 'Andrew Botz',
        hospital: 'Childrens Physicians',
        dischargeDates: '4/5/16 - 4/17/16',
        status: ReferralStatus.Yes,
        createdDate: moment()
    },
    {
        name: 'Ryan Rasmussan',
        hospital: 'AAA',
        dischargeDates: '4/5/16 - 4/7/16',
        status: ReferralStatus.No,
        createdDate: moment()
    },
    {
        name: 'Barry Mantilone',
        hospital: 'Creighton Medical',
        dischargeDates: '4/5/16 - 4/17/16',
        status: ReferralStatus.Considering,
        createdDate: moment().subtract(1, 'day')
    },
    {
        name: 'Jerry Herbawitz',
        hospital: 'Midlands Medical Center',
        dischargeDates: '4/5/16 - 4/7/16',
        status: ReferralStatus.Cancelled,
        createdDate: moment().subtract(1, 'day')
    },
    {
        name: 'Jeremy Chung',
        hospital: 'Bergan Mercy',
        dischargeDates: '4/5/16 - 4/7/16',
        status: ReferralStatus.Booked,
        createdDate: moment().subtract(1, 'day')
    },
    {
        name: 'Brian Harper',
        hospital: 'Bryan LGH West',
        dischargeDates: '4/5/16 - 4/7/16',
        status: ReferralStatus.Yes,
        createdDate: moment().subtract(2, 'day')
    }
];

        //     new Referral("Elva Lynch", "Bergan Mercy Medical Center", "4/8/16 - 4/12/16", ReferralStatus.Open, moment()),
        //     new Referral("Andrew Botz", "Children's Physicians", "4/5/16 - 4/7/16", ReferralStatus.Yes, moment()),
        //     new Referral("Ryan Rasmussan", "AAA", "4/5/16 - 4/7/16", ReferralStatus.No, moment()),
        //     new Referral("Barry Mantilone", "Creighton Medical", "4/5/16 - 4/7/16", ReferralStatus.Considering, moment().subtract(1, 'day')),
        //     new Referral("Jerry Herbawitz", "Midlands Medical Center", "4/5/16 - 4/7/16", ReferralStatus.Cancelled, moment().subtract(1, 'day')),
        //     new Referral("Jeremy Chung", "Bergan Mercy", "4/5/16 - 4/7/16", ReferralStatus.Booked, moment().subtract(1, 'day')),
        //     new Referral("Brian Harper", "Hospital", "4/5/16 - 4/7/16", ReferralStatus.Yes, moment().subtract(2, 'day')),