import {Planner} from './planner';

export class Patient {
    public id: number;
    public hospitalId: number;

    public mrn: string;
    public firstName: string;
    public lastName: string;
    public gender: string;
    public dateOfBirth: Date;
    public age: number;
    public admitDate: Date;
    public approximateDischargeStart: Date;
    public approximateDischargeEnd: Date;
    public dischargeNotes: string;
    public hospitalComments: string;
    public hospitalName: string;
    public location: string;

    public planner: Planner;
}