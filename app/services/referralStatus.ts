export enum ReferralStatus {
    Open,
    Yes,
    No,
    Considering,
    Booked,
    Cancelled
}