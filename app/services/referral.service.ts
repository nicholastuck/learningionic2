import {Injectable} from 'angular2/core';
import {Http, Response} from 'angular2/http';
import {Observable} from 'rxjs/Observable';

import {ReferralStatus} from './referralStatus';
import {Referral} from './referral';
import {REFERRALS} from './mockReferrals';


@Injectable()
export class ReferralService {

    constructor(private http: Http) { }


    getReferrals() {
        return REFERRALS.map(res => res);
    }

    handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server Error');
    }
}