import {Page, NavController, NavParams} from 'ionic-angular';
import {Observable} from 'rxjs/Observable';

import {PatientService} from './../../services/patient.service';
import {Patient} from './../../services/patient';

@Page({
    templateUrl: 'build/pages/patient/patient-details.html',
    providers: [PatientService]
})

export class PatientDetailsPage {

    patient: Patient;
    errorMessage: any;

    gitUsers: Observable<Object[]>;

    constructor(
        private _patientService: PatientService,
        private nav: NavController,
        navParams: NavParams) {
        // this.getPatient();
        this.getGitUsers();
    }

    // getPatient() {
    //     this._patientService.getPatient()
    //                         .subscribe(patient => this.patient = patient,
    //                                    error => this.errorMessage = <any>error);
    // }

    getGitUsers() {
        this.gitUsers = this._patientService.getGitUser();
    }

    refresh() {
        console.log('REFRESHING!!!!');
        this.gitUsers = this._patientService.getGitRandomUsers();
    }
}
