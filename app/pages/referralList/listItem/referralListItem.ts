import {Page, NavController, NavParams, Icon, Item} from 'ionic-angular';
import {ItemDetailsPage} from '../../item-details/item-details';
import * as moment from "moment";
import {Input, Component} from "angular2/core";
import {Referral} from "../../../services/referral";
import {ReferralStatus} from "../../../services/referralStatus";

@Component({
    selector: 'referral-list-item',
    templateUrl: 'build/pages/referralList/listItem/referralListItem.html',
    directives: [Item, Icon]
})
export class ReferralListItem {
    ReferralStatus:any = ReferralStatus;

    @Input() referral:Referral;

    constructor(private nav:NavController, navParams:NavParams) {

    }

    itemTapped(event, item) {
        //noinspection TypeScriptValidateTypes
        this.nav.push(ItemDetailsPage, {
            item: item
        });
    }
}
