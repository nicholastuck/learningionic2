import {Page, NavController, NavParams, Toolbar, ToolbarTitle, Toggle} from 'ionic-angular';
import {ItemDetailsPage} from '../item-details/item-details';

import {ReferralService} from './../../services/referral.service';
import {ReferralStatus} from "./../../services/referralStatus";
import {Referral} from "./../../services/referral";

import * as moment from "moment";
import {ReferralListItem} from "./listItem/referralListItem";
import {ReferralListCard} from "./card/referralListCard";


@Page({
    templateUrl: 'build/pages/referralList/referralList.html',
    providers: [ReferralService],
    directives: [ReferralListItem, ReferralListCard, Toggle]
})
export class ReferralListPage {

    selectedItem: any;
    errorMessage: string;
    useCards: boolean = false;

    ReferralStatus:any = ReferralStatus;
    referrals:Array<Referral>;
    momentCalendarDisplay = {
        lastDay: '[Yesterday]',
        lastWeek: '[Last] dddd (DD/MM/YY)'
    };

    constructor(
        private _referralService: ReferralService,
        private nav:NavController,
        navParams:NavParams) {

        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');

        this.referrals = this._referralService.getReferrals();
    }

    itemTapped(event, item) {
        //noinspection TypeScriptValidateTypes
        this.nav.push(ItemDetailsPage, {
            item: item
        });
    }
}
