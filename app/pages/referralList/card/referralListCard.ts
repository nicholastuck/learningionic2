import {Button, NavController, NavParams, Icon, Item, List} from 'ionic-angular';
import {ItemDetailsPage} from '../../item-details/item-details';
import * as moment from "moment";
import {Input, Component} from "angular2/core";
import {ReferralStatus} from "../../../services/referralStatus";
import {Referral} from "../../../services/referral";

@Component({
    selector: 'referral-list-card',
    templateUrl: 'build/pages/referralList/card/referralListCard.html',
    directives: [Item, Icon, Button, List]
})
export class ReferralListCard{
    ReferralStatus:any = ReferralStatus;

    @Input() referral:Referral;

    constructor(private nav:NavController, navParams:NavParams) {

    }

    itemTapped(event, item) {
        //noinspection TypeScriptValidateTypes
        this.nav.push(ItemDetailsPage, {
            item: item
        });
    }
}
